import React, { Component } from 'react'

import { UberhackMap, UberhackBox, UberhackBoxContent, UberhackPlace } from 'generics'
import { UberhackDivider } from 'generics'
import './Home.scss'

class Home extends Component {

  componentDidMount() {
    setTimeout(() => {
      this.goInternal(null, 'completed')
    }, 20000)
  }

  goInternal(event, path) {
    const { history } = this.props
    history.push(path)
  }

  _buildFooter() {
    return (
      <UberhackBoxContent>
        <p className="topTen">
          <span>
            com essa descoberta, <br />
            você vai conquistar <span className="bold orange">2 pontos.</span>
          </span>

          <br />
          <br />
          <em>dica de amigo:</em>
          <br />
          ao acumular 30 pontos, <br />
          você pode converter <br />
          em créditos no Bike POA. ;) <br />
        </p>
        <br />
        <span className="bold">vai lá, desbravador!</span>
      </UberhackBoxContent>
    )
  }

  render() {
    return (
      <div className="app-container">
        <UberhackMap />
        <UberhackBox footer={this._buildFooter()} arrow={true}>
          <UberhackBoxContent>
            <img height="30" src={require('img/binoculo-red.png')} />
            <span className="bold topTen">ora ora, temos um explorador <br /> de primeira viagem :D</span>
          </UberhackBoxContent>
          <UberhackDivider />
          <UberhackPlace complementar="sua primeira descoberta é" />
        </UberhackBox>
      </div >
    )
  }
}

export { Home }
