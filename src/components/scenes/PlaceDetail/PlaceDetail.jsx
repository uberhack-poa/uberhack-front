import React, { Component } from 'react'
import { UberhackPlace, UberhackBoxContent, UberhackInput, UberhackBox, UberhackMap, UberhackDivider, UberhackFeed } from '../../generics'
import { toastrService } from 'services'
import store from '../../../objects/store'

const marker = {
  lat: -30.027990,
  lng: -51.228052,
  color: '#3F785D',
}

export class PlaceDetail extends Component {
  state = {}

  _buildFooter() {
    return <UberhackFeed />
  }

  onChange = (event) => {
    this.setState({ value: event.target.value })
  }

  onSubmit = () => {
    if (!this.state.value) return
    store.push({ date: '19.05.19', text: this.state.value })
    this.setState({ value: '' })
    toastrService.success('Mensagem salva com sucesso')
  }

  render() {
    return (
      <div className="app-container">
        <UberhackMap marker={marker} />

        <UberhackBox footer={this._buildFooter()} arrow={true} big={true}>
          <UberhackBoxContent>
            <UberhackPlace densed={true} place={{ name: 'prefeitura de porto alegre' }} />
          </UberhackBoxContent>
          <UberhackBoxContent>
            <p> o edifício foi tombado pelo município em 21 de novembro de 1979 e passou por uma reforma total em 2003, adaptando diversos espaços internos para exposições de arte e para guarda do Acervo Artístico da Prefeitura de Porto Alegre.</p>
          </UberhackBoxContent>
          <UberhackDivider />
          <UberhackBoxContent>
            <span className="bold">compartilhe uma lembrança</span>
            <UberhackInput onSubmit={this.onSubmit} value={this.state.value} placeholder="fique tranquilo, é anônimo ;)" onChange={this.onChange} />
          </UberhackBoxContent>
        </UberhackBox>
      </div>
    )
  }
}
