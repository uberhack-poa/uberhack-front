import React, { Component } from 'react'
import Logo from 'img/logo.png'
import './UberhackLogo.scss'

class UberhackLogo extends Component {
  render() {
    return <img className="logo" src={Logo} alt="Desbrava" />
  }
}

export { UberhackLogo }
