import React, { Component, Fragment } from 'react'
import { UberhackBoxContent } from '../UberhackBoxContent'
import { UberhackPicture } from '../UberhackPicture'
import store from '../../../objects/store'

import './UberhackFeed.scss'

const people = [
  'https://images.unsplash.com/photo-1542103749-8ef59b94f47e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
  'https://images.unsplash.com/photo-1544723795-3fb6469f5b39?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=335&q=80',
  'https://images.unsplash.com/photo-1541943181603-d8fe267a5dcf?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=312&q=80',
]

export class UberhackFeed extends Component {

  _renderItem(item, index) {
    if (index > 1) return null
    return (
      <div className="topTen positioner" key={index}>
        <span className="goLeft">{item.date}</span>
        <p className="messages">{item.text}</p>
      </div>
    )
  }

  render() {
    return (
      <Fragment>
        <UberhackBoxContent otherClasses="zeroFlex">
          <p className="orange bold topTen">feed de lembranças</p>
        </UberhackBoxContent>
        <UberhackBoxContent className="background">
          {store.reverse().map((item, i) => this._renderItem(item, i))}
        </UberhackBoxContent>
        <UberhackBoxContent className="background">
          <p className="bold">quem também esteve aqui</p>
          <div className="peopleContainer topTen">
            {people.map((pe, i) => <UberhackPicture imgSrc={pe} key={i} />)}
            <div className="number"><span>+30</span></div>
          </div>
        </UberhackBoxContent>
      </Fragment>
    )
  }
}
