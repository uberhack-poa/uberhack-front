import React, { Fragment } from 'react'

import './UberhackPicture.scss'

const UberhackPicture = ({ text, imgSrc }) => {
  return (
    <Fragment>
      <div className="uberhackPicture">
        <img src={imgSrc} className="uberhackPicture__img uberhack-shadow"/>
        {text ? <h3 className="uberhackPicture__text">{text}</h3> : null }
      </div>
    </Fragment>
  )
}

export { UberhackPicture }
