import Loader from 'react-loader-spinner'
import React from 'react'
const UBERHACK_LOADER_ID = 'uberhack-loader'

import './UberhackLoader.scss'

const UberhackLoader = () => {
  return (
    <div id={UBERHACK_LOADER_ID}>
      <Loader
        type="Ball-Triangle"
        color="#00BFFF"
        height="100"
        width="100"
      />
    </div>
  )
}

UberhackLoader.show = () => {
  const loader = document.getElementById(UBERHACK_LOADER_ID)
  document.getElementsByClassName('App')[0].style.overflow = 'hidden'
  loader.style.display = 'flex'
}

UberhackLoader.hide = () => {
  const loader = document.getElementById(UBERHACK_LOADER_ID)
  document.getElementsByClassName('App')[0].style = ''
  loader.style.display = 'none'
}

export { UberhackLoader }
