import React from 'react'

import './UberhackBoxContent.scss'

export const UberhackBoxContent = ({ children, otherClasses = '' }) => (
  <div className={`boxContent ${otherClasses}`}>
    { children }
  </div >
)
