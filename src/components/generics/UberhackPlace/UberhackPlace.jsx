import React from 'react'
import { UberhackBoxContent } from '../UberhackBoxContent'

export const UberhackPlace = ({ densed, complementar, name = 'prefeitura de porto alegre' }) => (
  <UberhackBoxContent>
    <div className="placeContainer topTen">
      <span> {complementar} </span>
      <br />
      <img className="topTen" height={densed ? '45' : '50'} src={require('img/prefeitura.png')} />
      <p className="bold topTen green"> {name} </p>
    </div>
  </UberhackBoxContent>
)
