import FontAwesome from '@fortawesome/fontawesome'
import FontAwesomeFreeSolid from '@fortawesome/fontawesome-free-solid'
import FontAwesomeFreeBrands from '@fortawesome/fontawesome-free-brands'

FontAwesome.library.add(FontAwesomeFreeSolid, FontAwesomeFreeBrands)
