export default [
  {
    date: '18.05.19',
    text: 'Muitos momentos aqui, vim desde cedo com os meus avós. Reviver isso foi mágico :)',
  },
  {
    date: '18.05.19',
    text: 'Onde aprendi a gostar da Cidade, apesar de tudo. Tire 10 minutos olhando essa praça e ajude a ocupá-la. É um dos lugares mais bonitos de POA e é de todos nós…',
  },
]
